package com.atguigu.gulimail.gulimailproduct.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author Hern
 * @Date 2019-10-30 21:53
 * @Describle com.atguigu.gulimail.gulimailproduct.controller.HelloController
 */
@Controller
public class HelloController {

    @ResponseBody
    @RequestMapping(value = "ware")
    public String hello(){
        return "Hello, ware";
    }
}