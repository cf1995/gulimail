package com.atguigu.gulimail.gulimailproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GulimailProdApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailProdApplication.class, args);
    }

}
